import java.util.Comparator;


public class ProfitComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		double e1 = ((Company)o1).getProfit();
		double e2 = ((Company)o2).getProfit();
		if (e1 > e2) return 1;
		if (e1 < e2) return -1;
		return 0;
	}

}
