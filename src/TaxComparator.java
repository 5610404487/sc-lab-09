import java.util.Comparator;


public class TaxComparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		double e1 = ((Taxable)o1).getTax();
		double e2 = ((Taxable)o2).getTax();
		if (e1 > e2) return 1;
		if (e1 < e2) return -1;
		return 0;
	}

}
