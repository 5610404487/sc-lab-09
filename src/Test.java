import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {
	
	public static void main(String[] args){
		Test test = new Test();
		System.out.println("1.1 Person");
		test.testPerson();
		System.out.println("\n1.2 Product");
		test.testProduct();
		System.out.println("\n1.3 Company");
		test.testCompany();
		System.out.println("\n1.4 Tax");
		test.testTax();
	}
	
	public void testPerson(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("A",300000));
		persons.add(new Person("B",500000));
		persons.add(new Person("C",100000));

		System.out.println("before sort area in person");
		for (Person l : persons) 
			System.out.println(l);
		Collections.sort(persons);
		System.out.println("\nafter sort area in person");
		for (Person l : persons) 
			System.out.println(l);
	}
	public void testProduct(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("G",299));
		products.add(new Product("H",1990));
		products.add(new Product("I",590));
		System.out.println("before sort area in product");
		for (Product l : products) 
			System.out.println(l);
		Collections.sort(products);
		System.out.println("\nafter sort area in product");
		for (Product l : products) 
			System.out.println(l);
	}
	public void testCompany(){
		ArrayList<Company> companys = new ArrayList<Company>();
		companys.add(new Company("D",9000000,800000));
		companys.add(new Company("E",5000000,1000000));
		companys.add(new Company("F",3000000,20000));
		
		System.out.println("Before sort");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new EarningComparator());
		System.out.println("\nAfter sort with Earning");
		for (Company c : companys) {
			System.out.println(c);
		}
		Collections.sort(companys, new ExpenseComparator());
		System.out.println("\nAfter sort with Expense");
		for (Company c : companys) {
			System.out.println(c);
		}
		Collections.sort(companys, new ProfitComparator());
		System.out.println("\nAfter sort with Profit");
		for (Company c : companys) {
			System.out.println(c);
		}
	}
	public void testTax(){
		ArrayList<Taxable> all = new ArrayList<Taxable>();
		all.add(new Product("G",1500000));
		all.add(new Person("A",300000));
		all.add(new Company("D",90000,80000));
		System.out.println("Before sort");
		for (Taxable t : all) {
			System.out.println(t);
		}
		Collections.sort(all, new TaxComparator());
		System.out.println("\nafter sort Tax");
		for (Taxable t : all) 
			System.out.println(t);
	}
}