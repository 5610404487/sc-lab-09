

public class Product implements Taxable,Measurable,Comparable<Product>{
	private String name_pro;
	private int value;
	
	public Product(String name_pro,int value){
		this.name_pro = name_pro;
		this.value = value;
	}
	@Override
	public double getMeasure() {
		return this.value;
	}
	public double getTax() {
		double sum = 0;
		return sum = this.value*0.07;
	}
	public int compareTo(Product other) {
		// TODO Auto-generated method stub
		if (this.value < other.value ) { return -1; }
		if (this.value > other.value ) { return 1;  }
		return 0;
	}
	
	public String toString() {
		return "Product[name="+this.name_pro+", price="+this.value+", Tax="+getTax()+"]";
	}
}