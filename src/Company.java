

public class Company implements Taxable,Measurable{
	private String name_com;
	private int income;
	private int expense;
	
	public Company(String name_com, int income, int expense){
		this.name_com = name_com;
		this.income = income;
		this.expense = expense;
	}
	public String getName() {
		return name_com;
	}
	public int getEarning() {
		return income;
	}
	public int getExpense() {
		return expense;
	}
	public int getProfit() {
		return income-expense;
	}
	
	
	@Override
	public double getMeasure() {
		return this.income;
	}
	public double getTax() {
		double sum = 0;
		sum = this.income-this.expense;
		return sum = sum*0.3;
	}
	
	public String toString() {
		return "Company[name="+this.name_com+", earning="+this.income+", expense="+this.expense+", Profit="+getProfit()+", Tax="+getTax()+"]";
	}
	
	
}